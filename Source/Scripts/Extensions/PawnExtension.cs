﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;
using Verse.AI;
using Verse.AI.Group;
using RimWorld;
using rjw;
using Rimworld_Animations;
using HarmonyLib;

namespace Rimworld_Animations_Patch
{
    public static class PawnExtension
    {
        public static bool IsInBed(this Pawn pawn, out Building bed)
        {
			bed = pawn.Position.GetThingList(pawn.Map).FirstOrDefault(x => x is Building_Bed) as Building;
			return bed != null;
		}

        public static bool IsSeated(this Pawn pawn, out Building seat)
        {
			seat = pawn.Position.GetThingList(pawn.Map).FirstOrDefault(x => x is Building && x.def.building.isSittable) as Building;
			return seat != null;
        }

		public static bool IsHavingSex(this Pawn pawn)
		{
			if (pawn?.jobs?.curDriver == null || pawn.Dead || pawn.jobs.curDriver is JobDriver_Sex == false)
			{ return false; }

			JobDriver_Sex jobdriver = pawn.jobs.curDriver as JobDriver_Sex;

			return jobdriver.Partner != null && jobdriver.Partner != pawn;
		}

		public static bool IsMasturbating(this Pawn pawn)
		{
			if (pawn?.jobs?.curDriver == null || pawn.Dead || pawn.jobs.curDriver is JobDriver_Sex == false)
			{ return false; }

			JobDriver_Sex jobdriver = pawn.jobs.curDriver as JobDriver_Sex;

			return jobdriver.Partner == null || jobdriver.Partner == pawn || (jobdriver.Partner is Pawn) == false;
		}

		public static Pawn GetSexInitiator(this Pawn pawn)
		{
			if (pawn?.jobs?.curDriver != null && pawn.Dead == false && pawn.jobs.curDriver is JobDriver_SexBaseInitiator)
			{ return pawn; }

			JobDriver_SexBaseReciever jobDriver = pawn.jobs.curDriver as JobDriver_SexBaseReciever;

			if (jobDriver?.Partner?.jobs?.curDriver != null && jobDriver.Partner.Dead == false && jobDriver.Partner.jobs.curDriver is JobDriver_SexBaseInitiator)
			{ return jobDriver.Partner; }

			return null;
		}

		public static Pawn GetSexReceiver(this Pawn pawn)
		{
			if (pawn.jobs.curDriver is JobDriver_SexBaseReciever)
			{ return pawn; }

			JobDriver_SexBaseInitiator jobDriver = pawn.jobs.curDriver as JobDriver_SexBaseInitiator;

			if (jobDriver?.Partner?.jobs?.curDriver != null && jobDriver.Partner.Dead == false && jobDriver.Partner.jobs.curDriver is JobDriver_SexBaseReciever)
			{ return jobDriver.Partner; }

			return null;
		}

		public static Pawn GetSexPartner(this Pawn pawn)
		{
			return (pawn.jobs.curDriver as JobDriver_Sex)?.Partner;
		}

		public static List<Pawn> GetAllSexParticipants(this Pawn pawn)
		{
			List<Pawn> participants = new List<Pawn>();

			if (pawn?.jobs?.curDriver == null || (pawn.jobs.curDriver is JobDriver_Sex) == false)
			{ return participants; }
		
			if (pawn.GetSexReceiver() != null)
			{ 
				List<Pawn> partners = (pawn.GetSexReceiver().jobs.curDriver as JobDriver_SexBaseReciever).parteners;

				if (partners != null)
				{
					foreach (Pawn partner in partners)
					{
						if (partner != null)
						{ participants = partners; break; }
					}
				}
			}
			
			if (pawn.GetSexInitiator() != null)
			{
				Pawn partner = (pawn.GetSexInitiator().jobs.curDriver as JobDriver_SexBaseInitiator).Partner;
				
				if (partner != null && partner.Dead == false)
				{ participants.AddDistinct(partner); }
			}
		
			participants.AddDistinct(pawn);

			// Sort participants according to actorID, if they have one
			Dictionary<int, Pawn> _participants = new Dictionary<int, Pawn>();
			int c = 99;

			foreach (Pawn participant in participants)
			{
				ActorAnimationData data = pawn.GetAnimationData();

				if (data != null)
				{
					int idx = data.actorID;

					if (_participants.ContainsKey(data.actorID))
					{ DebugMode.Message("ERROR: Somehow you ended up with two actors with the same actorID"); idx = ++c; }

					_participants.Add(idx, participant);
				}

				else
				{ _participants.Add(++c, participant); }
			}

			participants = _participants.Values.ToList();

			//ActorAnimationData data = pawn.GetAnimationData();
			//if (data != null)

			//participants.SortBy(x => x.GetAnimationData() != null ? x.GetAnimationData().actorID : participants.IndexOf(x));

			return participants;
		}

		public static ActorAnimationData GetAnimationData(this Pawn pawn)
		{
			if (pawn.TryGetComp<CompBodyAnimator>() == null) return null;
			if (pawn.TryGetComp<CompBodyAnimator>().isAnimating == false) return null;

			AnimationDef animationDef = (AnimationDef)AccessTools.Field(typeof(CompBodyAnimator), "anim").GetValue(pawn.TryGetComp<CompBodyAnimator>());
			int actorID = (int)AccessTools.Field(typeof(CompBodyAnimator), "actor").GetValue(pawn.TryGetComp<CompBodyAnimator>());
			int currentStage = (int)AccessTools.Field(typeof(CompBodyAnimator), "curStage").GetValue(pawn.TryGetComp<CompBodyAnimator>());
			int stageTicks = (int)AccessTools.Field(typeof(CompBodyAnimator), "stageTicks").GetValue(pawn.TryGetComp<CompBodyAnimator>());
			Rot4 actorFacing = (Rot4)AccessTools.Field(typeof(CompBodyAnimator), "bodyFacing").GetValue(pawn.TryGetComp<CompBodyAnimator>());
			bool isMirrored = pawn.TryGetComp<CompBodyAnimator>().Mirror;

			return new ActorAnimationData(animationDef, actorID, currentStage, stageTicks, actorFacing, isMirrored);
		}
	}
}
