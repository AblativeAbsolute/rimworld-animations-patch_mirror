﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Verse;

namespace Rimworld_Animations_Patch
{
	public class BasicSettings : ModSettings
	{
		public static bool autoscaleDeltaPos = true;

		public static bool allowUndressing = true;
		public static bool clothesThrownOnGround = true;

		public static float undressingInPrivateDegree = 0.8f;
		public static float undressingInPublicDegree = 0.2f;

		public override void ExposeData()
		{
			base.ExposeData();
			Scribe_Values.Look(ref autoscaleDeltaPos, "autoscaleDeltaPos", true);
			Scribe_Values.Look(ref clothesThrownOnGround, "clothesThrownOnGround", true);
			Scribe_Values.Look(ref allowUndressing, "allowUndressing", true);
			Scribe_Values.Look(ref undressingInPrivateDegree, "undressingInPrivateDegree", 0.8f);
			Scribe_Values.Look(ref undressingInPublicDegree, "undressingInPublicDegree", 0.2f);
		}
	}

	public class BasicSettingsDisplay : Mod
	{

		public BasicSettingsDisplay(ModContentPack content) : base(content)
		{
			GetSettings<BasicSettings>();
		}

		public override void DoSettingsWindowContents(Rect inRect)
		{
			Listing_Standard listingStandard;

			listingStandard = new Listing_Standard();
			listingStandard.Begin(inRect);

			listingStandard.Gap(10f);
			listingStandard.Label("Misc Options");
			listingStandard.Gap(5f);
			listingStandard.CheckboxLabeled("      Auto-scale animations based on pawn body size", ref BasicSettings.autoscaleDeltaPos);

			listingStandard.Gap(10f);
			listingStandard.Label("Disrobing options");
			listingStandard.Gap(5f);
			listingStandard.CheckboxLabeled("      Animate pawn undressing", ref BasicSettings.allowUndressing);
			if (BasicSettings.allowUndressing)
			{
				listingStandard.CheckboxLabeled("      Show discarded clothes on the floor", ref BasicSettings.clothesThrownOnGround);
				listingStandard.Label("      Degree of disrobing when in bed (default is 0.80): " + BasicSettings.undressingInPrivateDegree.ToString("F"));
				listingStandard.Label("        " + ReportOnDisrobingInPrivate());
				BasicSettings.undressingInPrivateDegree = listingStandard.Slider(BasicSettings.undressingInPrivateDegree, 0f, 1f);
				listingStandard.Label("      Degree of disrobing when out of bed (default is 0.20): " + BasicSettings.undressingInPublicDegree.ToString("F"));
				listingStandard.Label("        " + ReportOnDisrobingInPublic());
				BasicSettings.undressingInPublicDegree = listingStandard.Slider(BasicSettings.undressingInPublicDegree, 0f, 1f);
			}

			listingStandard.End();

			base.DoSettingsWindowContents(inRect);
		}

		public string ReportOnDisrobing(float val)
		{
			string report;

			if (val == 1) { report = "(Pawns will always fully disrobe)"; }
			else if (val >= 0.6667) { report = "(Pawns prefer to fully disrobe)"; }
			else if (val >= 0.3333) { report = "(Pawns like to keep some clothes on)"; }
			else if (val > 0) { report = "(Pawns will keep most of their clothes on)"; }
			else { report = "(Pawns will try to keep all of their clothes on)"; }

			return report;
		}

		public string ReportOnDisrobingInPrivate()
		{
			return ReportOnDisrobing(BasicSettings.undressingInPrivateDegree);
		}

		public string ReportOnDisrobingInPublic()
		{
			return ReportOnDisrobing(BasicSettings.undressingInPublicDegree);
		}

		public override string SettingsCategory()
		{
			return "Rimworld Animations Patch - Basic settings";
		}
	}
}
